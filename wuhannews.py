import requests
import re
from bs4 import BeautifulSoup
import os


def cnanews():
    dic={}
    url="https://www.channelnewsasia.com"
    r = requests.get("https://www.channelnewsasia.com/news/topics/wuhan-virus")
    soup = BeautifulSoup(r.content,"lxml")
    count=0
    for a in soup.find_all('a', class_='teaser__title',href=True):
        title=clean(a)
        lis=[title,url+a["href"]]
        if not title.endswith("Video"):
            dic[str(count)]=lis
            count+=1
    return dic

def clean(msg):
    clean = re.compile('<.*?>')
    m = re.sub(clean, '', str(msg)).replace("\n"," ").strip()
    return  m


def getNews(lis):
    url=lis[1]
    r=requests.get(url)
    so=BeautifulSoup(r.content,"lxml")
    k=0;
    txt="\n=======================================\n"+lis[0]+"\n=======================================\n\n"
    for p in so.find_all('p'):
        if k!=0 and not p.has_attr("class"):
            p=clean(p)
#            print(p)
            if p.isupper():
                p="\n"+p+"\n"
            txt=txt+p+"\n"
        k+=1
    os.system('clear')  # For Linux/OS X
    print(txt)
    input()

def getinput(dic):
    for i in range(len(dic)):
        print ("Choice : "+str(i+1)+"\nTitle : "+dic[str(i)][0]+"\nLink : "+dic[str(i)][1]+"\n=============================")
    value=input("What is your choice? ")
    val=str(int(value)-1)
    if val in dic:
        getNews(dic[val])
    else:
        print("no such number")
while True:
    dic=cnanews()
    getinput(dic)
